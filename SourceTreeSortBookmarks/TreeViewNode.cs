﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SourceTreeSortBookmarks
{
    class TreeViewNode
    {
        public bool IsExpanded { get; set; }
        public bool IsLeaf { get; set; }
        public String Name { get; set; }
        public String Path { get; set; }
        public String RepoType { get; set; }
    }
}
