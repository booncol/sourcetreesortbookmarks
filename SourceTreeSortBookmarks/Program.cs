﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SourceTreeSortBookmarks
{
    class Program
    {
        static void Main(string[] args)
        {
            String path = System.IO.Path.Combine(Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData), "Atlassian", "SourceTree");
            String xmlPath = System.IO.Path.Combine(path, "bookmarks.xml");
            String oldPath = System.IO.Path.Combine(path, "bookmarks.xml.old");

            List<TreeViewNode> tvnodes = new List<TreeViewNode>();

            {
                XmlDocument doc = new XmlDocument();
                doc.Load(xmlPath);

                XmlNode root = doc.SelectSingleNode("ArrayOfTreeViewNode");
                if (root != null)
                {
                    XmlNodeList nodes = root.SelectNodes("TreeViewNode");
                    foreach (XmlNode node in nodes)
                    {
                        TreeViewNode tvn = new TreeViewNode();

                        tvn.IsExpanded = bool.Parse(node.SelectSingleNode("IsExpanded").InnerText);
                        tvn.IsLeaf = bool.Parse(node.SelectSingleNode("IsLeaf").InnerText);
                        tvn.Name = node.SelectSingleNode("Name").InnerText;
                        tvn.Path = node.SelectSingleNode("Path").InnerText;
                        tvn.RepoType = node.SelectSingleNode("RepoType").InnerText;

                        tvnodes.Add(tvn);
                    }
                }

                if (System.IO.File.Exists(oldPath))
                {
                    System.IO.File.Delete(oldPath);
                }

                System.IO.File.Move(xmlPath, oldPath);
            }

            tvnodes.Sort(delegate(TreeViewNode p1, TreeViewNode p2)
            {
                return p1.Name.CompareTo(p2.Name);
            });

            {
                XmlDocument doc = new XmlDocument();
                XmlNode docNode = doc.CreateXmlDeclaration("1.0", null, null);
                doc.AppendChild(docNode);

                XmlElement root = doc.CreateElement("ArrayOfTreeViewNode");
                root.SetAttribute("xmlns:xsd", @"http://www.w3.org/2001/XMLSchema");
                root.SetAttribute("xmlns:xsi", @"http://www.w3.org/2001/XMLSchema-instance");

                foreach (TreeViewNode tvn in tvnodes)
                {
                    XmlElement node = doc.CreateElement("TreeViewNode");
                    XmlAttribute attr = doc.CreateAttribute("xsi", "type", "http://www.w3.org/2001/XMLSchema-instance");
                    attr.Value = "BookmarkNode";
                    node.Attributes.Append(attr);

                    XmlElement isExpandedNode = doc.CreateElement("IsExpanded");
                    isExpandedNode.AppendChild(doc.CreateTextNode(tvn.IsExpanded.ToString().ToLower()));
                    node.AppendChild(isExpandedNode);

                    XmlElement isLeafNode = doc.CreateElement("IsLeaf");
                    isLeafNode.AppendChild(doc.CreateTextNode(tvn.IsLeaf.ToString().ToLower()));
                    node.AppendChild(isLeafNode);

                    XmlElement nameNode = doc.CreateElement("Name");
                    nameNode.AppendChild(doc.CreateTextNode(tvn.Name));
                    node.AppendChild(nameNode);

                    node.AppendChild(doc.CreateElement("Children"));

                    XmlElement pathNode = doc.CreateElement("Path");
                    pathNode.AppendChild(doc.CreateTextNode(tvn.Path));
                    node.AppendChild(pathNode);

                    XmlElement typeNode = doc.CreateElement("RepoType");
                    typeNode.AppendChild(doc.CreateTextNode(tvn.RepoType));
                    node.AppendChild(typeNode);

                    root.AppendChild(node);
                }

                doc.AppendChild(root);
                doc.Save(xmlPath);
            }
        }
    }
}
