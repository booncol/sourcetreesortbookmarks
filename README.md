**SourceThreeSortBookmarks** is a tiny console application to sort your Atlassian SourceTree bookmarks by name.

* Language: **C#**
* Platform: **PC-Windows**